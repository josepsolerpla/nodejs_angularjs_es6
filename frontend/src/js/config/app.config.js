import authInterceptor from './auth.interceptor'

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider) {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);

  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'layout/app-view.html',
    controller: 'appCtrl',
    resolve: {
      auth: function(User) {
        return User.verifyAuth();
      },
      eventsName: function(Events){
        return Events.getAll().then(
          (array) => array,
        );
      }
    }
  });

  $urlRouterProvider.otherwise('/');

}

export default AppConfig;

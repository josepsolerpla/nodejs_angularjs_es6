function loginConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.auth', {
      url: '/login',
      controller: 'loginCtrl',
      templateUrl: 'login/login.html',
      title: 'login',
      resolve: {
        type: function(){
          return "login";
        },
        userNames: function(User){
          return User.getAllUsernames().then(
            (User) => User,
            (err) => $state.go('app.home')
          );
        },
        auth: function(User) {
          return User.ensureAuthIsNot(false);
        }
      }
    })
    .state('app.register', {
      url: '/register',
      controller: 'loginCtrl',
      templateUrl: 'login/login.html',
      title: 'register',
      resolve: {
        type: function(){
          return "register";
        },
        userNames: function(User){
          return User.getAllUsernames().then(
            (User) => User,
            (err) => $state.go('app.home')
          );
        },
        auth: function(User) {
          return User.ensureAuthIsNot(false);
        }
      }
    })
  };
  
  export default loginConfig;
  
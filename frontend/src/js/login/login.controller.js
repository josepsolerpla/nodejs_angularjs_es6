//Controller for mdDialog of login
//This is launched at the same time as the login Component so the functionality
//Is on the loginComponent
class loginCtrl {
    constructor(AppConstants, $scope, $mdDialog, $state, type, userNames, Comments) {
      'ngInject';
      $scope.userNames = userNames;
      this.appName = AppConstants.appName;
      $scope._$state = $state;
      $scope.dialogType = type;

      //Close Dialog
      $scope.closeDialog = function() {
        $mdDialog.hide();
      };
    }
}

export default loginCtrl;
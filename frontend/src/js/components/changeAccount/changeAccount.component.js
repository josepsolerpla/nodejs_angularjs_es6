class changeAccountCtrl {
  constructor(AppConstants, $scope, User) {
    'ngInject';
    this.User_ = User;
    this.selectedUser = null;
  }
  /*
    $changeAccount

    This will take the current user and change with the selectedUser
  */
  changeAccount(){
    let user = this.selectedUser/*.split("\\")*/;
    this.User_.changeUser(user);
  }
}

let changeAccount = {
  controller: changeAccountCtrl,
  templateUrl: 'components/changeAccount/changeAccount.tmp.html',
  bindings:{
    account: "="
  }
};

export default changeAccount;
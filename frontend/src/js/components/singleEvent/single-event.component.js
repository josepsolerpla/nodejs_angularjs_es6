class singleEventCtrl {
    constructor(AppConstants, $scope, $state, $mdMenu, $mdDialog) {
      'ngInject';
      }
  }
  
  let singleEvent = {
    controller: singleEventCtrl,
    templateUrl: 'components/singleEvent/single-event.html',
    bindings: {
      event: '=',
    }
  };
  
  export default singleEvent;
//Component made for login/Register menu 
//Is displayed on loginDialog and login controller page app.login and app.register
class mapLoadCtrl {
    constructor(AppConstants, $scope, $state, NgMap) {
      'ngInject';
      $scope.heightMap = "300px";
      this.googleMapsUrl = 'https://maps.google.com/maps/api/js?v=3.20&client=AIzaSyChNptO3D0G8twfsWUi6wn0mJjeoQztyQY';

      /**
       * $showInfo
       * 
       * Function used to open Details when clicked on map
       */
      $scope.currentEventTarget = {};
      $scope.showInfo = function(e, event) {
        $scope.currentEventTarget = event;
        $scope.map.showInfoWindow('myInfoWindow', this);
      }
    }
    
}

let loginForm = {
  controller: mapLoadCtrl,
  templateUrl: 'components/mapLoad/mapLoad.tmp.html',
  bindings: {
      array: '=',
      functionButton: '&',
      focus: '=',
  }
};

export default loginForm;
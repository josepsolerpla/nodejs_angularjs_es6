import angular from 'angular';

let componentsModule = angular.module('app.components', []);

//Default :

import ListErrors from './list-errors.component'
componentsModule.component('listErrors', ListErrors);

import ShowAuthed from './show-authed.directive';
componentsModule.directive('showAuthed', ShowAuthed);

import FollowBtn from './buttons/follow-btn.component';
componentsModule.component('followBtn', FollowBtn);

import ArticleMeta from './article-helpers/article-meta.component';
componentsModule.component('articleMeta', ArticleMeta);

import FavoriteBtn from './buttons/favorite-btn.component';
componentsModule.component('favoriteBtn', FavoriteBtn);

import ArticlePreview from './article-helpers/article-preview.component';
componentsModule.component('articlePreview', ArticlePreview);

import ArticleList from './article-helpers/article-list.component';
componentsModule.component('articleList', ArticleList);

import ListPagination from './article-helpers/list-pagination.component';
componentsModule.component('listPagination', ListPagination);

import changeAccount from './changeAccount/changeAccount.component';
componentsModule.component('changeAccount', changeAccount);

//MY COMPONENTS
//Events:

//Component used for listing events
import eventList from './events-list/events-list.component';
componentsModule.component('eventList',eventList);

//              !!I SHOULD BRING HERE THE SINGLE EVENT !!
import singleEvent from './singleEvent/single-event.component';
componentsModule.component('singleEvent',singleEvent);

//Component used for display Login/Register
import loginForm from './loginForm/loginForm.component';
componentsModule.component('loginForm',loginForm);

//Component used for display Google Maps
import mapLoad from './mapLoad/mapLoad.component';
componentsModule.component('mapLoad',mapLoad);

export default componentsModule;

class eventListCtrl {
  constructor(AppConstants, $scope, $state, $mdMenu, $mdDialog) {
    'ngInject';
        this.focus = null;

        /*
          This will open the modal of mdDialog

          I have setted the controler inside the function because i only use 1 function
        */
        $scope.openDetails = function(target){
            $mdDialog.show({
              controller: function DialogController($scope, $mdDialog, event) {
                $scope.event = event;
                this.closeDialog = function() {
                  $mdDialog.hide();
                };
              },
              templateUrl: 'templates/eventsDialog.tmp.html',
              parent: angular.element(document.body),
              targetEvent: this.event,
              locals:{
                event: target
              },
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
          }
    }
}

let eventList = {
  controller: eventListCtrl,
  templateUrl: 'components/events-list/events-list.html',
  bindings: {
    events: '=',
    filter: '=',
    openDetails: '&'
  }
};

export default eventList;
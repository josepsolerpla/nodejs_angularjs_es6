//Component made for login/Register menu 
//Is displayed on loginDialog and login controller page app.login and app.register
class loginFormCtrl {
    constructor(AppConstants, $scope, $state, $mdMenu, $mdDialog, User, Toaster) {
      'ngInject';

      this.formData = {

      }
      this._toaster = Toaster;
      this.User_ = User;
      this.Scope = $scope;
      this._$state  = $state;
      this._$mdDialog = $mdDialog;
      $scope.title = $state.current.title;

      }
      //This is activated when client submit formulary it chceck on this.authType if 
      //is login or register
      submitForm() {
        this.isSubmitting = true;
        this.User_.attemptAuth(this.authType, this.Scope.formData).then(
          (res) => {
            //Close Modal and go to Home
            this._$mdDialog.hide()
            this._toaster.showToaster('success','Successfully Logged In');
            this._$state.go('app.home');
          },
          (err) => {
            this.isSubmitting = false;
            console.log(err)
            this._toaster.showToaster('error','Error trying to login');
            this.errors = err.data.errors;
          }
        )
      }
      //Validate if array contains this name
      //
      validateArrayValue(name,array){
        var back = false;
        if(array){
          if(array.length != 0){
            array.forEach(function (res){
                if (res == name) {
                    back = true;
                };
            });
        }
      }
        
        return back;
      }
    }

let loginForm = {
  controller: loginFormCtrl,
  templateUrl: 'components/loginForm/loginForm.tmp.html',
  bindings: {
      authType: '=',
      userNames: '='
  }
};

export default loginForm;
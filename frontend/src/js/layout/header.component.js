class AppHeaderCtrl {
  constructor(AppConstants, User, $scope, Events, $state,$mdMenu, $stateParams) {
    'ngInject';
    this.appName = AppConstants.appName;
    this.currentUser = User.current;
    this.User_ = User;
    //console.log(this.currentUser)
    $scope.$watch('User.current', (newUser) => {
      this.currentUser = newUser;
    })
    $scope.openDetails = function(event){
      if(event){$mdMenu.hide();;$state.go('app.eventsdetails', {name:event.name,id: event._id})}
    }
  }
  longinNormal(){
    this.User_.loginDialog("login");
  }
  singIn(){
    this.User_.loginDialog("register");
  }
  logOut(){
    this.User_.logout();
  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html',
  bindings: {
    search: '=',
  }
};

export default AppHeader;

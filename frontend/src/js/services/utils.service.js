export default class Utils {
  constructor($http, $state, $q) {
    'ngInject';
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;
  }
    /*
      $validateArrayValue

      Validate if array contains this name
    */
   validateArrayValue(name,array){
    var back = false;
    if(array){
      if(array.length != 0){
        array.forEach(function (res){
            if (res == name) {
                back = true;
            };
        });
      }
    }
    return back;
  }
}


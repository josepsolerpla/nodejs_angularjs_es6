import loginCtrl from '../login/login.controller';
export default class User {
  constructor(JWT, AppConstants, $http, $state, $q, $mdDialog, Comments) {
    'ngInject';
    this.Comments = Comments
    this.mdDialog = $mdDialog;
    this._JWT = JWT;
    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;

    this.current = null;  //Declared the app variable to get the current account in use

  }
  /*
    $userFind

    Service to find in Mongod users by parameters and things
  */
  userFind(parameter,search) {
    let deferred = this._$q.defer();
    this._$http({
      url:  this._AppConstants.api + '/userSearch',
      method: 'POST',
      data: { 
        parameter: parameter,
        search: search,
      }
    }).then(
      (res) => {
        deferred.resolve(res.data.users);
      },
      (err)=>{
          deferred.resolve(false);
      }
    )
    return deferred.promise
  }
  userFindbyId(id) {
    let deferred = this._$q.defer();
    this._$http({
      url:  this._AppConstants.api + '/userSeachID',
      method: 'POST',
      data: { 
        id: id,
      }
    }).then(
      (res) => {
        deferred.resolve(res.data.users);
      },
      (err)=>{
          deferred.resolve(false);
      }
    )
    return deferred.promise
  }
  /*
    $getAllUSernames

    This will return an array with the usernames emails that are actually already used
    This uses $userFind
  */
  getAllUsernames(){
    let deferred = this._$q.defer();
      let search = {salt:{$ne:null}};
      let parameter = {email:1,"_id":0};
    
      this.userFind(parameter,search).then((userNames)=>{
          deferred.resolve(this.Comments.returnArrayByObject("email",userNames))
      })
    return deferred.promise;
  }
  /*
    $changeUSer

    Service used to change User from Current Account
    Im using if parameter on assign the name to set Scocial if it is one 
  */
 changeUser(newUser = null){
  this.current.associatedUsernames.forEach((obj)=> {
    if(obj.idUser == newUser){
      this.current.username = obj.idsocial ? obj.user+" \\Facebook" : obj.user;
    }
  }); //This will change all app curent username selected
  this.current.currentUserId = newUser  
  console.log(this.current)
 }
  /*
    $loginDialog

    Service to load Login Modal on the page 
    Is using the component Login/Register"
  */
  loginDialog(type_){
    this.mdDialog.show({
      controller: loginCtrl,
      locals:{
        type: type_,
        userNames: this.getAllUsernames(),
      },
      templateUrl: 'login/loginDialog.tmp.html',
      parent: angular.element(document.body),
      targetEvent: this.event,
      clickOutsideToClose:true,
    })
  }
  /*
    $auth BOTH

    Service used when Client try to Authentificate on any point
    of the app
  */
  attemptAuth(type, credentials) {
    let route = '/'+ type;
    return this._$http({
      url: this._AppConstants.api + '/users' + route,
      method: 'POST',
      data: {
        user: credentials
      }
    }).then(
      (res) => {
        console.log("Login / Register")
        this._JWT.save(res.data.user.token);
        this.current = res.data.user;
        return res;
      }
    );
  }
  /*
    $authSocial
  */
  socialAuth(type, credentials) {
    let route = '/'+ type;
    console.log(route);
    return this._$http({
      url: this._AppConstants.api + route,
      method: 'POST',
      data: {
        user: credentials
      }
    }).then(
      (res) => {
        this._JWT.save(res.data.user.token);
        console.log(this._JWT.get())
        this.current = res.data.user;
        return res;
      },
      (err) => {
        this._JWT.destroy();
        deferred.resolve(false);
      }
    );
  }
  /*
    $update

    Service to update info on the account
  */
  update(fields) {
    return this._$http({
      url:  this._AppConstants.api + '/user',
      method: 'PUT',
      data: { user: fields }
    }).then(
      (res) => {
        this.current = res.data.user;
        return res.data.user;
      }
    )
  }
  /*
    $logout

    Service to delete the JWT and close session
  */
  logout() {
    this.current = null;
    this._JWT.destroy();
    console.log("LOGOUT")
    this._$state.go(this._$state.$current, null, { reload: true });
  }
  /*
    $verifyAuth

    All thre ways to verify and be ensure is authenticated on the app
  */
  verifyAuth() {
    let deferred = this._$q.defer();
    
    if(!this._JWT.get()){
      deferred.resolve(false);
      return deferred.promise;
    }

    if (this.current) {
      this._JWT.save(this.current.token);
      deferred.resolve(true);
    } else {
        // IF TOKEN EXIST 
        this._$http({
          url: this._AppConstants.api + '/user',
          method: 'GET',
          headers: {
            Authorization: 'Token ' + this._JWT.get()
          }
        }).then(
          (res) => {
            this.current = res.data.user;
            this._JWT.save(res.data.user.token);
            deferred.resolve(true);
          },

          (err) => {
            this._JWT.destroy();
            deferred.resolve(false);
          }
        )
    }
    return deferred.promise;
  }
  verifySocialAuth() {
    let deferred = this._$q.defer();
      this._$http({
        url: this._AppConstants.api + '/social',
        method: 'GET',
      }).then(
        (res) => {
          this.current = res.data.user;
          this._JWT.save(res.data.user.token);
          deferred.resolve(true);
        },
        (err) => {
          this._JWT.destroy();
          deferred.resolve(false);
        }
      )

      return deferred.promise;
  }
  ensureAuthIs(bool) {
    let deferred = this._$q.defer();

    this.verifyAuth().then((authValid) => {
      if (authValid !== bool) {
        this._$state.go('app.home')
        deferred.resolve(false);
      } else {
        deferred.resolve(true);
      }

    });

    return deferred.promise;
  }
  ensureAuthIsNot(bool) {
    let deferred = this._$q.defer();

    this.verifyAuth().then((authValid) => {
      if (authValid || !this.current) {
        deferred.resolve(false);
      } else {
        this._$state.go('app.home')
        deferred.resolve(true);
      }
    });

    return deferred.promise;
  }
}

export default class Events {
  constructor(JWT, AppConstants, $http, $q, Toaster) {
    'ngInject';
    this._$q = $q;
    this._AppConstants = AppConstants;
    this._$http = $http;
    this.Toaster = Toaster;
  }

/*
$getAll

Get all Events on mongo database
*/
getAll() {
let deferred = this._$q.defer();
this._$http({
  url: this._AppConstants.api + '/events',
  method: 'GET',
}).then(
  (res) => {
    deferred.resolve(res.data.events)
  },
  (err) =>{
    deferred.resolve(false)
  }
);
return deferred.promise;
}
/*
$getEvent

Get 1 event using his Id
*/
getEvent(id) {
let deferred = this._$q.defer();
this._$http({
  url: this._AppConstants.api + '/events/'+ id,
  method: 'GET',
}).then(
  (res) => {
    deferred.resolve(res.data.events)
  },
  (err) =>{
    deferred.resolve(false)
  }
);
return deferred.promise;
}
/*
$createEvent

Method used to create Events and validate
*/
createEvent(Event_){
if(Event_.name == undefined){return {err:"Name not defined"}} //Validate Name
if(Event_.description == undefined){return {err:"Description not defined"}} //Validate Description
if(Event_.owner == undefined){return {err:"The Owner must be choosed"}} //Validate Owner

let deferred = this._$q.defer();
this._$http({
  url: this._AppConstants.api + '/events/createevent',
  method: 'POST',
  data: { 
    event: Event_,
  }
}).then(
  (res) => {
    if(res.data.err){
      this.Toaster.showToaster('error',res.data.err)
      deferred.resolve(false)
    }
    deferred.resolve(res.data.event)
  },
  (err) =>{
    deferred.resolve(false)
  }
);
return deferred.promise;
}
}

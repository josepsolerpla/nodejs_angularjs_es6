class eventCreateCtrl {
  constructor(User, events, Toaster, Utils, Events) {
    'ngInject';
      this.Utils_ = Utils;
      this.User_ = User;
      this.inputs = [0]
      this.arrayArtists = []
      this.toaster = Toaster;
      this.Event_Service = Events;
      this.Events = events.map((obj)=>{return obj.name});
    }
    /*
      $moreInputs

      Insert input on the html
    */
    moreInputs(){
      this.inputs.push(0)
    }
    /*
      $verifyId

      Check / Verify if the id on the Input is valid or not
    */
    verifyId(id){
      this.User_.userFindbyId(id).then((userNames)=>{
        if(userNames){  //Check if User exist or not
          let artist = {email:userNames.email,username:userNames.users[0].username,id:userNames.users[0]._id};
          if(this.arrayArtists.length == 0){  //If array its empty introduce it
            this.arrayArtists.push(artist)
          }else{
            this.arrayArtists.forEach((obj)=>{
              if(id != artist.id){  //Check if this artist is already inserted
                this.arrayArtists.push(artist)
                this.toaster.showToaster('info',"User has been added")
              }else{
                this.toaster.showToaster('error',"Already inserted")
              }
            });//End forEach
          }
        }else{
          this.toaster.showToaster('error',"User hasn't been found, check the ID")
        }
      }).catch((err)=>{
          this.toaster.showToaster('error',"Error")
      });
    }
    /* 
      $createEvent
    */
   createEvent(){
    let Event_ = this.createForm;
    Event_.artists = this.arrayArtists;
    Event_.owner = this.User_.current.currentUserId;

    this.Event_Service.createEvent(Event_).then(
      (res)=>{
        if(res){
          this.toaster.showToaster("info","Event Created Correctly")
          
        }
      }),
      (err)=>{
        this.toaster.showToaster("error","Error on Create Events, Try it later")
      }

   }
  }
  
  export default eventCreateCtrl;
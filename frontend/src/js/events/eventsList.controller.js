class eventsListCtrl {
  constructor(AppConstants, $scope, $stateParams, events, $mdDialog) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.filtro = $stateParams.filter;
    this.filteredEvents = [];
    this.Events = events;
  }
}

export default eventsListCtrl;
class eventsDetailsCtrl {
  constructor(AppConstants, $scope, Events, $state, $stateParams, Toaster, event) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.Event = event;
    this.arrayEvent = [event]

    $scope.event = this.Event;
  }
}

export default eventsDetailsCtrl;
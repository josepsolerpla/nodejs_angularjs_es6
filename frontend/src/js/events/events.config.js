function eventsConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.eventslist', {
    url: '/events',
    controller: 'eventsListCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'events/events.list.html',
    title: 'Events',
    resolve: {
      events: function(Events, $state, $stateParams) {
        return Events.getAll().then(
          (Events) => Events,
          (err) => $state.go('app.home')
        )
      }
    }
  })
  .state('app.eventsdetails', {
    url: '/events/:name/:id',
    controller: 'eventsDetailsCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'events/events.details.html',
    title: 'DetailEvent',
    resolve: {
      event: function(Events, $state, $stateParams) {
        return Events.getEvent($stateParams.id).then(
          (Events) => Events,
          (err) => $state.go('app.home')
        )
      }
    }
  })
  .state('app.eventscreate',{
    url: '/events/create',
    controller: 'eventCreateCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'events/eventCreate.html',
    title: 'CreateEvents',
    resolve:{
      auth: function(User) {
        return User.ensureAuthIs(true);
      },
      events: function(Events) {
        return Events.getAll().then(
          (Events) => Events,
        )
      }
    }
  })
};

export default eventsConfig;

import angular from 'angular';

// Create the module where our functionality can attach to
let eventsModule = angular.module('app.events', []);

// Include our UI-Router config settings
import eventsConfig from './events.config';
eventsModule.config(eventsConfig);


// Controllers
import eventsListCtrl from './eventsList.controller';
eventsModule.controller('eventsListCtrl', eventsListCtrl);

import eventsDetailsCtrl from './eventsDetails.controller';
eventsModule.controller('eventsDetailsCtrl', eventsDetailsCtrl);

import eventCreateCtrl from './eventCreate.controller';
eventsModule.controller('eventCreateCtrl',eventCreateCtrl);

export default eventsModule;
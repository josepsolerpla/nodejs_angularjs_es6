import User from "../services/user.service";

function HomeConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.home', {
    url: '/',
    controller: 'HomeCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'home/home.html',
    title: 'Home',
    resolve: {
      events: function(Events, $state, $stateParams) {
        return Events.getAll().then(
          (Events) => Events,
          (err) => $state.go('app.home')
        )
      },
      auth: function(User) {
        return User.verifySocialAuth();
      }
    }
  });

};

export default HomeConfig;

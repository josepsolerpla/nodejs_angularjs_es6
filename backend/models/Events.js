var mongoose = require('mongoose');

var EventsSchema = new mongoose.Schema({
  name: String,
  desc: String,
  type: String,
  place: String,
  latitud: String,
  longitud: String,
  fecha: String,
  price: String,
  img: String,
  stock: Number,
  owner: String,
  artists: Array
}, {timestamps: true});

mongoose.model('Events', EventsSchema);


var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = require('../config').secret;

var UserSchema = new mongoose.Schema({
  idsocial: String,
  username: {type:String, unique: false, index: false},
  bio: String,
  image: String,
  type: {type:String,default:'normal'},
}, {timestamps: true});

var AccountSchema = new mongoose.Schema({
  //  ID
  email: {type:String, unique: true, index: true},
  users: [UserSchema],
  hash: String,
  salt: String,
}, {timestamps: true});


AccountSchema.plugin(uniqueValidator, {message: 'is already taken.'});

AccountSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

AccountSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

AccountSchema.methods.generateJWT = function() {
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    id: this._id,
    username: this.users.username,
    exp: parseInt(exp.getTime() / 1000),
  }, secret);
};

AccountSchema.methods.toAuthJSON = function(){
  return {
    username: this.users[0].username,
    email: this.email,
    id: this.id,
    idsocial: this.users[0].idsocial,
    token: this.generateJWT(),
    bio: this.users[0].bio,
    image: this.users[0].image,
    associatedUsernames : this.users.map((obj)=>{return {user:obj.username,idsocial:obj.idsocial,idUser:obj._id}}),
    currentUserId: this.users[0]._id,
  };
};

AccountSchema.methods.toProfileJSONFor = function(user){
  return {
    username: this.users.username,
    bio: this.users.bio,
    image: this.users.image || 'https://static.productionready.io/images/smiley-cyrus.jpg',
  };
};


mongoose.model('Account', AccountSchema);

/*
  NOT USED : 

UserSchema.methods.favorite = function(id){
  if(this.favorites.indexOf(id) === -1){
    this.favorites.push(id);
  }

  return this.save();
};

UserSchema.methods.unfavorite = function(id){
  this.favorites.remove(id);
  return this.save();
};

UserSchema.methods.isFavorite = function(id){
  return this.favorites.some(function(favoriteId){
    return favoriteId.toString() === id.toString();
  });
};

UserSchema.methods.follow = function(id){
  if(this.following.indexOf(id) === -1){
    this.following.push(id);
  }

  return this.save();
};

UserSchema.methods.unfollow = function(id){
  this.following.remove(id);
  return this.save();
};

UserSchema.methods.isFollowing = function(id){
  return this.following.some(function(followId){
    return followId.toString() === id.toString();
  });
};
*/
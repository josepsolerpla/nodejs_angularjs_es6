var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var Account = mongoose.model('Account');
var auth = require('../auth');

/*  $user
*/
router.get('/user', auth.required, function(req, res, next){
  Account.findById(req.payload.id).then(function(account){
    if(!account){ return res.sendStatus(401); }

    return res.json({user: account.toAuthJSON()});
  }).catch(next);
});

/*  $social
    USE THIS ROUTE TO GET THE STORED DATA OF SESSIONSTORE, DELETE IT AND THEN RETURN THE JSON
*/
router.get('/social', function(req, res, next){
  //Function to delete the SessionStore
  function deleteStore(){
    var store = req.sessionStore;    
    for(var sid in store.sessions){ 
      store.destroy(sid, function (err, dat) {}
    )}
  }
  let sessions = req.sessionStore.sessions;
  let sessionUser;
  for(var key in sessions){sessionUser = (JSON.parse(sessions[key]).passport.user)};  //Parse all the data into a sessionUser
  if(sessionUser == undefined){return res.sendStatus(404)}  //If its undefined send 404 Not Found

  Account.findOne({"_id":sessionUser.id}).then(function(account){
    deleteStore() //Delete de sessionStored
    if(!account) return res.sendStatus(401);
    user = account.users.filter(obj => {
      return obj.idsocial == sessionUser.idsocial
    })
    account.users = [user]
    return res.json({user: account.toAuthJSON()});
  }).catch(next);
});

/*  $user
    CHECK IF USER IS CORRECT
*/
router.put('/user', auth.required, function(req, res, next){
  Account.findById(req.payload.id).then(function(account){
    if(!account){ return res.sendStatus(401); }
    user = account.users.filter((obj)=>{
      return obj.username == req.body.user.username; 
    })
    // only update fields that were actually passed...
    if(typeof req.body.user.username !== 'undefined'){
      user.username = req.body.user.username;
    }
    if(typeof req.body.user.email !== 'undefined'){
      user.email = req.body.user.email;
    }
    if(typeof req.body.user.bio !== 'undefined'){
      user.bio = req.body.user.bio;
    }
    if(typeof req.body.user.image !== 'undefined'){
      user.image = req.body.user.image;
    }
    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
    }

    return account.save().then(function(){
      return res.json({user: account.toAuthJSON()});
    });
  }).catch(next);
});

/*  $Login
  THIS IS USED FROM MENU BAR LOGIN DIALOG AND LOGIN PAGE
  Use passport localy to authenticate in mongo user
  CARE PASSPORT LOCAL HAS TO BE INCLUDEED ON APP.js
*/
router.post('/users/login', function(req, res, next){
  if(!req.body.user.email)
    return res.status(422).json({errors: {email: "can't be blank"}});
  if(!req.body.user.password)
    return res.status(422).json({errors: {password: "can't be blank"}});
    
  passport.authenticate('local', {session: false}, function(err, user, info){
      if(user){
      user.token = user.generateJWT();
      return res.json({user: user.toAuthJSON()});
    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});

/*  $Register

*/
router.post('/users/register', function(req, res, next){
  let account = new Account();
  let user = {
    username: req.body.user.username,
  }
  account.email = req.body.user.email;
  account.setPassword(req.body.user.password);
  account.users = [user];
  
  Account.findOne({"email":account.email,"users.idsocial":{$ne:null}},function(err, accounts){
    if(accounts){
      accounts.users.push(user);
      accounts.setPassword(req.body.user.password);
      account = accounts;
    }
    account.save().then(function(){
      return res.json({user: account.toAuthJSON()});
    }).catch(next);
  })
});

//  $users
//Same as register WHY ? Dont touch it maybe the app will break
router.post('/users', function(req, res, next){
  let user = new Account();
  //USE THIS ON POSTMAN  let info_ = JSON.parse(req.body.user);
  let info_ = req.body.user;
  user.username = info_.username;
  user.email = info_.email;
  user.setPassword(info_.password);

  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

/*  $usersarch
    Get all users depending a parameter
*/
router.post('/userSearch', function(req, res, next){
  let parameter_ = req.body.parameter;
  let search_ = req.body.search;
  Account.find(search_,parameter_).then(function(user_,info){
    if(user_){
      return res.json({users:user_});
    }else{
      return res.status(422).json(info);
    }
  })
});
/*
  $userSeachID
  Search especific by id
*/
router.post('/userSeachID', function(req, res, next){
  let ObjectId = require('mongoose').Types.ObjectId;
  let id = req.body.id;
  Account.find({users:{$elemMatch:{"_id":ObjectId(id)}}},{users:{$elemMatch:{"_id":ObjectId(id)}},hash:0}).then(function(user_,info){
    if(user_){
      return res.json({users:user_[0]});
    }else{
      return res.status(422).json(info);
    }
  })
});
//PASSPORT AUTH 
//
router.get('/auth/googleplus', 
  passport.authenticate('google', { scope: [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'] })
);
router.get('/auth/googleplus/callback',
  passport.authenticate('google', {
   successRedirect : 'http://localhost:8080/#!/',
   failureRedirect: '/' })
  );
module.exports = router;

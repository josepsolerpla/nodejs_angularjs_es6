var router = require('express').Router();
var mongoose = require('mongoose');
var Events = mongoose.model('Events');

router.get('/', function(req, res, next) {
    Events.find().then(function(events){
      console.log("Find Events");
    return res.json({events: events});
  }).catch(next);
});

router.get('/:id', function(req, res, next) {
  Events.findById(req.params.id).then(function(events){
    if(!events){ return res.sendStatus(401); }
    console.log("Return Event");
  return res.json({events: events});
  }).catch(next);
});

router.post('/createevent', function(req, res, next) {
  let eventIn = req.body.event;
  Events.find({name:eventIn.name}).then(function(event){
    if(event.length >= 1){ 
      return res.json({err:"Name is already taked"}); 
    }else{
      let newEvent = new Events;

      newEvent.desc = eventIn.description;
      newEvent.name = eventIn.name;
      newEvent.latitud = eventIn.latitud;
      newEvent.longitud = eventIn.longitud;
      newEvent.artists = eventIn.artists;
      newEvent.owner = eventIn.owner;

      newEvent.save().then((event)=>{
        return res.json({event:newEvent})
      }).catch((error)=>{
        return res.json({err:error})
      })
    }
  }).catch(next);
return "entra";
});

module.exports = router;
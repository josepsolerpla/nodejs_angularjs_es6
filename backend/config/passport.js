var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Account = mongoose.model('Account');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var socialKeys = require('../credentials/credentials.json');

passport.serializeUser((user, done) => {
  done(null, {id:user.id,idsocial:user.idsocial});
});

passport.deserializeUser((id, done) => {
   //console.log(`id: ${id}`);
   Account.findById(id)
    .then(user => {
      done(null, user);
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
});

//Passport strategy to use localy $LOCALSTRATEGY

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]'
}, function(email, password, done) {
  Account.findOne({email: email,salt:{$ne:null}}).then(function(account){
    if(!account || !account.validPassword(password)){
      return done(null, false, {errors: {'email or password': 'is invalid'}});
    }else{
        return done(null, account);
    }
  }).catch(done);
}));


//Passport strategy to connect with Facebook $FACEBOOK

passport.use(new GoogleStrategy({
  clientID: socialKeys.GOOGLEPLUS_CLIENT_ID,
  clientSecret: socialKeys.GOOGLEPLUS_CLIENT_SECRET,
  callbackURL: socialKeys.GOOGLEPLUS_CALLBACK,
  passReqToCallback: true
  },function(req, accessToken, refreshToken, profile, done) {
    Account.findOne({ 'email' : profile.emails[0].value }, function(err, account) {
      if (err)
        return done(err);
      if(account){                                      //IF ACCOUNT EXISTS
        var user = account.users.filter(obj => {        //FILTER THE ACCOUNT TO GET THE USER THAT I WANT
          return obj.idsocial == profile.id
        })
        if(user.length == 1) {                                      //IF USER EXIST
          account.users = [user];                       //ASSIGN THE USER TO ACCOUNT
          account = account.toAuthJSON();
          return done(null, account, "User exists");
        }else{                      
            newUser = {                                   //CREATE THE NEW USER
              idsocial: profile.id,
              username: profile.name.givenName,
              bio: profile.photos[0].value,
            }
            account.users.push(newUser)                //ADD NEWUSER TO THE ACCOUNT
            account.save(function(err,user) {
              if(err){
                console.log(err);
                return done(null, null, "Error creating user");
              }
              user = account.users.filter(obj => {
                return obj.idsocial == profile.id
              })
              account.users = [user]
            return done(null,account.toAuthJSON(), "User has been created");
          });
        }//END IF USER
      }else{
        let newAccount = new Account();
          newAccount.email = profile.emails[0].value;
          newAccount.users = [{                         //CREATE THE NEW USER
            idsocial: profile.id,
            username: profile.name.givenName,
            bio: profile.photos[0].value,
          }]
        newAccount.save(function(err,account) {
          if(err){
            console.log(err);
              return done(null, null, "Error creating user");
          }
          newAccount = newAccount.toAuthJSON();
          return done(null,newAccount , "User has been created");
        });
      }//END IF ACCOUNT
    });
  }
));
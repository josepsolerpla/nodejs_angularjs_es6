FRAMEWORK

Hi, this is a NodeJS & AngularJS Framework, the application is a basic Event Searcher called Ekkoo.

Backend :

Backend is build in NodeJS, it has 3 points for entry Contact, Events & Users(Accounts), the app is already commented you can't find with "$" example : $userSearchID.

-Contact will receive a base entry point that will send a automated email based on configuration that Frontend will provide.

The function to send the Email is made on @sendgrid.

-Event has 3 entry points, the base one will return all events that Frontend can filter and store to reuse, ":id" that also will return the event but this time you can find a ID (This will be used for example on Details on each Event), and last but not least "/createevent" this will validate and create a Event Schema from Moongo DB.

-Users (Accounts), this route provide the Login/Register System and Searches for various things(validation, details, profile, data).

Let's put an example to be more clear, when a user wants to register all the app will have in advance the email of each account, on this way the use will have faster response if he want to register/login, for this point the call will be the first one $user work as a findAll, then you have two options, login, that the entry will be $login, this will validate the data and use Passport locally to authenticate, then you have register, the entry will be $Register, also validate the data and if all its correct will create a base Account with your USer.
On this app you can also Login/Register on a Social Way, i have used Google+, Google will provide use with the information and then will come back to the $social entry point that will check if user exist and if not will create the account and user.

The app is build around Accounts, because i want each Client can access the app on differents way, because i think it will be easier to communicate between the differents accounts types. Talking about this i want to explain this system.

This app will have Normal User, Artist, Agent and Seller.
Differents roles will acces you to differents features of the app providing you with chats between Agents and Sellers, Artists finders, allways to make more visible your role to those people that could be interested on your job.

Frontend :

The Frontend is build on AngularJS, and has been builted in two ways, the app will provide some entry points that will print Components and will provide a SPA that also will use this compontents.

Explain the whole application will be hard, because of this, im going to talk about the main functionalities.

Components :

On the app i have tried to use components using a methodology, re use everything you can. The app has a few components : 

-ChangeAccounts, like i have said before a account can have differents users this component provide the functionality to change this users.

-EventsList, this will provide a list and will print for eache the next component.

-singleEvent, this will all the information of each Event object that is provided but with different style (CSS) depending on class.

-mapLoad, like the name said this will print the map, markers and information depending on the Focus setted and array of Events sended.

-loginForm, this will provide the same template and will change between login and register, and the whole system is configured on this. 

To make the frontend easier to understand i provide with comments the whole framework.